using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletScript : MonoBehaviour
{
    Vector3 direction;
    Rigidbody rb;

    [SerializeField] float power;

    private void Awake()
    {
        rb = GetComponent<Rigidbody>();
    }

    private void FixedUpdate()
    {
        rb.velocity = direction * power;
    }

    public void Direction(Vector3 target)
    {
        direction = target - transform.position;
    }

    private void OnCollisionEnter(Collision collision)
    {
        if(collision.collider.CompareTag("Cenary"))
        {
            Destroy(gameObject);
        }
        
    }
}
