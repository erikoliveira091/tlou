using Cinemachine;
using StarterAssets;
using System.Collections;
using UnityEngine;
using UnityEngine.Windows;

public class Gun : MonoBehaviour
{
    [SerializeField] LayerMask aimColliderLayerMask;
    private Ray ray;
    private RaycastHit hit;

    [SerializeField] GameObject bullet;
    [SerializeField] bool canFire;

    ShotEffects shootEffects;
    [SerializeField] Transform shotSpawn;
    [SerializeField] Transform shellSpawn;

    [SerializeField] CinemachineVirtualCamera cam;

    [SerializeField] float velocity;

    private ThirdPersonController thirdPersonController;
    private StarterAssetsInputs starterAssetsInputs;

    [SerializeField] AudioSource audioSource;

    void Awake()
    {
        shootEffects = GetComponent<ShotEffects>();
        thirdPersonController = GetComponent<ThirdPersonController>();
        starterAssetsInputs = GetComponent<StarterAssetsInputs>();
        canFire = true;
    }

    void Update()
    {

        Vector3 mouseWorldPosition = Vector3.zero;
        Vector2 screenCenterPoint = new Vector2(Screen.width / 2f, Screen.height / 2f);

        ray = Camera.main.ScreenPointToRay(screenCenterPoint);

        if(Physics.Raycast(ray, out hit, 999f, aimColliderLayerMask))
        {
            mouseWorldPosition = hit.point;
        }

        if(starterAssetsInputs.aim)
        {
            thirdPersonController._animator.SetBool("Aim", true);
            if(starterAssetsInputs.fire && canFire)
            {
                StartCoroutine(Fire());
            }
            cam.Priority = 0;
        }
        else
        {
            thirdPersonController._animator.SetBool("Aim", false);
            cam.Priority = 30;
        }

        if (starterAssetsInputs.dance)
        {
            thirdPersonController._animator.SetBool("Special", true);
        }
        else
        {
            thirdPersonController._animator.SetBool("Special", false);
        }
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawLine(shotSpawn.position, hit.point);

    }

    IEnumerator Fire()
    {
        canFire = false;
        audioSource.Play();
        shootEffects.MuzzleFlash(shotSpawn.position, shotSpawn.rotation);
        shootEffects.Shell(shellSpawn.position, shellSpawn.rotation);
        GameObject si = Instantiate(bullet, shotSpawn.position, shotSpawn.rotation);
        si.GetComponent<BulletScript>().Direction(hit.point);
        Destroy(si.gameObject, 5f);

        yield return new WaitForSeconds(0.5f);

        canFire = true;
    }
}
